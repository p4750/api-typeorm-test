import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ClienteEntity } from './interfaces/cliente.entity';
import { Cliente } from './interfaces/cliente.interface';

@Injectable()
export class ClientesService {

    constructor(
        @InjectRepository(ClienteEntity)
        private readonly clienteRepository: Repository<ClienteEntity>) {}

    async criarCliente(cliente: Cliente): Promise<Cliente> {
        return await this.clienteRepository.save(cliente)
    }

    async consultarClientes(): Promise<Cliente[]> {
        return await this.clienteRepository.find()
    }

    async consultarClientePeloId(idCliente: number): Promise<Cliente> {
        return await this.clienteRepository.findOne(idCliente)
    }

    async atualizarCliente(idCliente: number, cliente: Cliente): Promise<void> {
        await this.clienteRepository.update(idCliente, cliente)
    }

    async deletarCliente(idCliente: number): Promise<void> {
        await this.clienteRepository.delete(idCliente)
    }

}
