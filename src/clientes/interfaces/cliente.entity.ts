import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('clientes')
export class ClienteEntity {

    @PrimaryGeneratedColumn()
    id_cliente: number;

    @Column({ default: '' })
    nome: string;

    @Column({ default: '' })
    telefone: string;

}