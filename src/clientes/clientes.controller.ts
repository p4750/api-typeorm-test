import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { ClientesService } from './clientes.service';
import { Cliente } from './interfaces/cliente.interface';

@Controller('api/clientes')
export class ClientesController {

    constructor(private readonly clientesService: ClientesService) {}

    @Post()
    async criarCliente(
        @Body() cliente: Cliente): Promise<Cliente> {
        return await this.clientesService.criarCliente(cliente)
    }

    @Get()
    async consultarClientes(): Promise<Cliente[]> {
        return await this.clientesService.consultarClientes()
    }

    @Get('/:id')
    async consultarClientePeloId(
        @Param('id') idCliente: number): Promise<Cliente> {
        return await this.clientesService.consultarClientePeloId(idCliente)
    }
    
    @Put('/:id')
    async atualizarCliente(
        @Body() cliente: Cliente,
        @Param('id') idCliente: number): Promise<void> {
        await this.clientesService.atualizarCliente(idCliente, cliente)
    }

    @Delete('/:id')
    async deletarCliente(
        @Param('id') idCliente: number): Promise<void> {
        await this.clientesService.deletarCliente(idCliente)
    }

}
